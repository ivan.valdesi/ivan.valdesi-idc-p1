import json
from application.models import Order, Piece
from application import db

import application.order_logic


def test_get_orders(test_order_svc, test_database):
    client = test_order_svc.test_client()
    resp = client.get("/order")
    assert resp.status_code == 200


"""

Test de la nueva funcionalidad 

"""

TEST_ORDER = {
    "number_of_pieces": 3,
    "description": "test",
    "client_id": 2,
    "country": "araba",
}


def test_get_order(test_order_svc, test_database, monkeypatch):
    def mock_create_order(self, description, country, number_of_pieces, client_id):
        session = db.session
        new_order = None
        new_order = Order(
            description=description,
            country=country,
            number_of_pieces=number_of_pieces,
            client_id=client_id,
            status=Order.STATUS_ACCEPTED,
        )
        session.add(new_order)
        for i in range(new_order.number_of_pieces):
            piece = Piece()
            piece.status = Piece.STATUS_CREATED
            piece.order = new_order
            session.add(piece)

        session.commit()
        order_id = new_order.id
        session.close()

        return order_id

    def mock_authenticate(self, recv_jwt):
        return True, {"name": "test_user"}

    def mock_check_permission(self, dec_jwt, permission):
        return True

    monkeypatch.setattr(
        application.order_logic.OrderLogic, "create_order", mock_create_order
    )
    monkeypatch.setattr(
        application.authenticator.Authenticator, "authenticate", mock_authenticate
    )
    monkeypatch.setattr(
        application.authenticator.Authenticator,
        "check_permission",
        mock_check_permission,
    )

    client = test_order_svc.test_client()

    resp = client.post(
        "/order",
        data=json.dumps(TEST_ORDER),
        headers={"Authorization": "Bearer " + ""},
        content_type="application/json",
    )
    assert resp.status_code == 200
    resp = client.get("/order")
    assert resp.status_code == 200
    # Check there is only one order in /order
    assert len(json.loads(resp.get_data(as_text=True))) == 1

    resp = client.get("/order/1")
    assert resp.status_code == 200
    # Check there is only one order in /order/1
    assert json.loads(resp.get_data(as_text=True))["status"] == Order.STATUS_ACCEPTED
