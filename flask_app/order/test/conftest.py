import pytest
from application import create_app as create_app_order
from application import db, models


@pytest.fixture(scope="session", autouse=True)
def test_order_svc():
    app = create_app_order()
    with app.app_context():
        yield app


"""
    Recreate db on each test
"""


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.drop_all(db.engine)
    models.Base.metadata.create_all(db.engine)
