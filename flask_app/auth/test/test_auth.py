import json


def test_view_clients(test_auth_svc, test_database, add_user):
    client = test_auth_svc.test_client()
    resp = client.get("/auth/client")
    content = resp.get_data(as_text=True)
    assert resp.status_code == 200
    assert len(json.loads(content)) == 0
    add_user("test", "1234")
    resp = client.get("/auth/client")
    content = resp.get_data(as_text=True)
    assert len(json.loads(content)) == 1
    assert json.loads(content)[0] == "test"


def test_check_empty(test_auth_svc, test_database):
    client = test_auth_svc.test_client()
    resp = client.get("/auth/client")
    content = resp.get_data(as_text=True)
    assert resp.status_code == 200
    assert len(json.loads(content)) == 0
