import pytest
from application import create_app as create_app_auth
from application import db, models
from application.auth_logic import AuthLogic


@pytest.fixture(scope="session", autouse=True)
def test_auth_svc():
    app = create_app_auth()
    with app.app_context():
        yield app


"""
    Recreate db on each test
"""


@pytest.fixture(scope="function")
def test_database():
    models.Base.metadata.drop_all(db.engine)
    models.Base.metadata.create_all(db.engine)


@pytest.fixture(scope="module")
def add_user():
    def _add_user(username, passwd):
        logic = AuthLogic()
        logic.create_client(username, passwd)

    return _add_user
