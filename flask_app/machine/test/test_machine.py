import json


def test_view_status(test_machine_svc, test_database):
    client = test_machine_svc.test_client()
    resp = client.get("/machine/status")
    assert resp.status_code == 200
