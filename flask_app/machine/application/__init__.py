from flask import Flask

from .config import Config
from .BLConsul import BLConsul
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(Config)
    db.init_app(app)

    with app.app_context():
        from . import routes
        from . import models

        models.Base.metadata.create_all(db.engine)
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        return app
