import json


def test_perfrom_deposit(test_payment_svc, test_database):
    client = test_payment_svc.test_client()
    resp = client.post(
        "payment/perform_deposit",
        data=json.dumps({"clientId": 2, "quantity": 50}),
        content_type="application/json",
    )
    assert resp.status_code == 200


def test_get_payment(test_payment_svc, test_database):
    client = test_payment_svc.test_client()
    # Add 50 to client 2
    resp = client.post(
        "payment/perform_deposit",
        data=json.dumps({"clientId": 2, "quantity": 50}),
        content_type="application/json",
    )
    assert resp.status_code == 200

    # get client 2 payment

    resp = client.get("/payment/2")
    assert resp.status_code == 200

    content = resp.get_data(as_text=True)
    assert content == "Client 2 has 50.0$"
