import json


def test_get_deliveries(test_delivery_svc, test_database):
    client = test_delivery_svc.test_client()
    resp = client.get("/delivery")
    assert resp.status_code == 200
