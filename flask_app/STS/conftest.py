import pytest
import requests
from test_smoke import URL_CREATE_USER, new_client


@pytest.fixture(scope="session", autouse=True)
def create_test_user():
    print(URL_CREATE_USER)
    requests.post(URL_CREATE_USER, verify=False, json=new_client)
