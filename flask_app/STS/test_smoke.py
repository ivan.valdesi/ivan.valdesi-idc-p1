import requests
from os import environ
import json
import re
import time


# CONST
HOST = environ.get("HOST_IP", "")
PORT = "8443"
USERNAME = "sts_user"
PASSWORD = "1234"

# URLS

URL_CREATE_USER = "https://{}:{}/auth/client".format(HOST, PORT)
URL_AUTHENTICATE = "https://{}:{}/auth".format(HOST, PORT)
URL_PAYMENT = "https://{}:{}/payment/perform_deposit".format(HOST, PORT)
URL_ORDER = "https://{}:{}/order".format(HOST, PORT)


new_client = {"username": USERNAME, "password": PASSWORD}


payment_deposit = {"clientId": 2, "quantity": 50}


def get_order_dict():

    new_order = {
        "number_of_pieces": 3,
        "description": "test",
        "client_id": 2,
        "country": "araba",
    }

    return new_order


def test_create_order(create_test_user):

    # Get  token
    response = requests.get(URL_AUTHENTICATE, auth=(USERNAME, PASSWORD), verify=False)

    assert response.json()["jwt"] != ""

    token = response.json()["jwt"]

    header = {"Authorization": "Bearer " + token}

    # Create Payment

    response = requests.post(URL_PAYMENT, verify=False, json=payment_deposit)

    assert response.json()["success"] != "true"

    # Create Order

    response = requests.post(
        URL_ORDER, verify=False, json=get_order_dict(), headers=header
    )

    assert response.text.startswith(
        "Authenticated as sts_user with C OWN ORDER permissions"
    )


def test_create_order_fails(create_test_user):

    invalid_token = "invalid jwt example"

    header = {"Authorization": "Bearer " + invalid_token}

    # Create Order

    response = requests.post(
        URL_ORDER, verify=False, json=get_order_dict(), headers=header
    )

    assert response.text.startswith("Bad Authentication")


def test_cancel_order(create_test_user):

    # Get  token
    response = requests.get(URL_AUTHENTICATE, auth=(USERNAME, PASSWORD), verify=False)

    assert response.json()["jwt"] != ""

    token = response.json()["jwt"]
    header = {"Authorization": "Bearer " + token}

    # Create Payment

    response = requests.post(URL_PAYMENT, verify=False, json=payment_deposit)
    assert response.json()["success"] != "true"

    # Create Order

    response = requests.post(
        URL_ORDER, verify=False, json=get_order_dict(), headers=header
    )

    regex = re.compile("Order saga (.*) started")
    res = regex.search(response.text)
    order_id = res.group(1)

    # Now Cancel Order after 5 secs
    time.sleep(5)
    cancel_order_url = URL_ORDER + "/" + str(order_id)

    response = requests.delete(cancel_order_url, verify=False, headers=header)
    assert response.status_code == 200

    response = requests.get(cancel_order_url, verify=False)
    assert response.status_code == 200
