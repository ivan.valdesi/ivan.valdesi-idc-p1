#!/bin/bash
set -e

echo "Before running this script run docker login to registry.gitlab.com"


export AUTH_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/auth:latest
export DELIVERY_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/delivery:latest
export MACHINE_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/machine:latest
export ORDER_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/order:latest
export PAYMENT_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/payment:latest
export LOGGER_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/logger:latest
export HAPROXY_PROD_IMAGE=registry.gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/haproxy:latest

docker-compose -f docker-compose.prod.yml pull 
docker-compose -f docker-compose.prod.yml up -d consul haproxy rabbitmq auth
sleep 60
docker-compose -f docker-compose.prod.yml up -d
