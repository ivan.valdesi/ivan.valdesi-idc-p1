#!/bin/bash


COV_FILE="./coverage.txt"
exec >> >(tee $COV_FILE ) 2>&1
printf "COVERAGE\n\n"


printf "\n\nOrder\n\n"
docker-compose -f docker-compose.test.yml exec -T order  python3 -m pytest -p no:warnings -s test --cov="application"
printf "\n\nAuth\n\n" 
docker-compose -f docker-compose.test.yml exec -T auth  python3 -m pytest -p no:warnings -s test --cov="application"
printf "\n\nPayment\n\n" 
docker-compose -f docker-compose.test.yml exec -T payment  python3 -m pytest -p no:warnings -s test --cov="application"
printf "\n\nMachine\n\n" 
docker-compose -f docker-compose.test.yml exec -T machine  python3 -m pytest -p no:warnings -s test --cov="application"
printf "\n\nDelivery\n\n" 
docker-compose -f docker-compose.test.yml exec -T delivery  python3 -m pytest -p no:warnings -s test --cov="application"


AVG_COV=$(cat coverage.txt | grep TOTAL | sed -E 's/^.*(.[0-9]{1,3}%)/\1/' | sed "s/%//" | awk '{ SUM += $1} END { print SUM/5 }')

echo "The total coverage is $AVG_COV"
