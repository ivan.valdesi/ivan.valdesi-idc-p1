# **P1 -** **Validated** **Container** **Delivery** **- Iván Valdés Irigoyen**
Este repositorio contiene la **Práctica 1** denominada **Validated Container Delivery** de la asignatura **Integración y Despliegue Continuo** del máster [Análisis de datos, Ciberseguridad y Computación en la nube](https://www.mondragon.edu/es/master-universitario-analisis-datos-ciberseguridad-computacion-nube) 

La práctica tiene por objetivo ser capaz de **diseñar e implementar un pipeline CI** para el sistema de pedidos (“Orders”). 

## Project Information

### Pipelines status

**Branch master**:

![](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/badges/master/pipeline.svg)

**Branch dev**:

![](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/badges/dev/pipeline.svg)


### Test coverage average of **all** microservices :

**Branch master**:

[![](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/badges/master/coverage.svg)](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/pipelines/129956417/builds)

**Branch dev**:

[![](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/badges/dev/coverage.svg)](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/pipelines/129949547/builds)


*Nota: Es posible clickar en las badges de coverage para ver el job del que se ha extraido el valor. (En el stage de Test)*




## Getting started 

Esta aplicación ha sido desplegada en AWS. Es posible acceder a la aplicación mediante esta [url](https://{{host}}:8443/machine/status)

***Nota**: La instancia esta suspendida de momento. Contactad conmigo si es necesario levantarla.*

## Changelog

Con el desarrollo de esta práctica se ha liberado una nueva release del proyecto Orders. A continuación, los principales cambios:

* Test suite de **pruebas unitarias** con el que se proporciona una cobertura alrededor **55%**.
* **Smoke Test Suite** para el proyecto. Tests de integración de las sagas.
* Nueva **funcionalidad**  en el servicio Order: GET order/\<id\>.
* Se ha diseñado, implementado y desplegado el **pipeline CI** que pasa los test unitarios, la STS y calcula el coverage.
* **Despliegue manual** de la aplicación en AWS.
* Se ha gestionado el desarrollo en base a la **metodología ágil** descrita en `doc.md`


## Documentation

Los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica quedan recogidos en la documentación complementaria [doc.md](doc/doc.md).

## Authors

- Iván Valdés Irigoyen [ivan.valdesi@mondragon.edu](mailto:ivan.valdesi@mondragon.edu)

En la implementación del código original también formaron parte:

- Alexander Tesouro [alexander.tesouro@alumni.mondragon.edu](mailto:alexander.tesouro@alumni.mondragon.edu)
- Iker Ocio [iker.ocio@alumni.mondragon.edu ](mailto:iker.ocio@alumni.mondragon.edu )


##  License
This project is licensed under the [MIT License](https://choosealicense.com/licenses/mit/),  see the `LICENSE.md` file for details.