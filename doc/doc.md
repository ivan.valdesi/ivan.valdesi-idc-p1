# Documentación

En este fichero se incluyen todos los detalles de las especificaciones, diseño y evidencias del desarrollo de la práctica 1 **Validated Container Delivery**. 

Lo primero es indicar el punto de partida del cual se ha comenzado el proyecto. En mi caso, debido a que los commits del proyecto del pbl no garantizaban el correcto funcionamiento de la aplicación. Se ha optado por utilizar el último commit del siguiente repositorio [link](https://gitlab.danz.eus/ivan.valdesi/servicesapp_popbl). Para añadir una nueva funcionalidad se ha eliminado una funcionalidad y recreado mediante TDD como se explica más adelante. 


A continuación se repasan los diferentes puntos de la rúbrica descibiendo el punto alcanzado en cada apartado.


## Metodología ágil en Gitlab 

### **Definición de los artefactos "agile"**

El primer paso consistió en la definición de los milestone a partir del enunciado de la práctica. En un principio eran los siguientes:


- M1 - Tests unitarios
- M2 - Desarrollo de los Smoke Tests
- M3 - Nueva Funcionalidad con TDD
- M4 - Creación del pipeline CI

Durante el desarrollo del trabajo, caí en la cuenta de que no tenía sentido estar hasta el final sin pipeline ya que resulta más fácil ir integrando los tests en los stages poco a poco. Además permite ver el desarrollo de una funcionalidad con TDD de forma más clara. Por otro lado se ha añadido un último milestone para preparar la entrega y la documentación de la práctica así como el despliegue. El resultado final es el siguiente:

- M1 - Tests unitarios
- M2 - Creación del pipeline CI
- M3 - Desarrollo de los Smoke Tests
- M4 - Nueva Funcionalidad con TDD
- M5 - Terminar documentación y preparar entrega 

Durante ejercicios previos he probado con diferentes tipos de board. Para la práctica, al estar trabajando una única persona, se ha optado por un único board con las listas open, to-do, doing,close.

![a](img/board.png)

En estas listas se han añadido issues correspondientes a las tareas a realizar.


### **Flujo de trabajo**

Debido a que se trata de un proyecto en el que solo existe un desarrollador, he decidido seguir un gitworkflow simplificado. He mantenido la rama master como la rama para versiones funcionales de la aplicación que podrían ponerse en producción. Para desarrollar, he creado una rama dev donde he ido almacenando los diferentes commits realizados. Cuando un commit tenía relación con una issue en concreto, pero no se llegaba a completar la tarea, lo mencionaba haciendo uso de la keyword "Related to". De la misma manera cuando un issue completa una funcionalidad un commit he utilizado Closes. La issues siempre se cierran en el merge request. El merge de la rama dev a master marca el fin de un milestone.

![](img/graph.png)

El flujo de trabajo ha sido el siguiente.

- Comenzar definiendo los issues correspondientes al milestone. Las issues de un milestone son flexibles. Se han ido añadiendo o modificando a medida que han sido necesarias para el milestone.
- Se van realizando commits. Siempre intentando usar las keyword related to o closes.
- Cuando el milestone estaba finalizado se hacía un merge de la rama dev a la rama master. Cerrando todos los issues.


### Exclusiones
- No se actualiza el README y la doc en cada merge. Soy consciente de que es lo correcto pero se ha dejado la doc para el final por motivos de tiempo.
- En la puesta a punto del pipeline de CI, es inevitable tener un gran grupo de commits que arreglan typos. Estos commits no siguen las buenas prácticas del texto de commit.

## Test suite de pruebas unitarias


### **Cambio en la arquitectura**

Nuestra aplicación hacia uso de la librería SQL-ALCHEMY como ORM. Para poder testear la aplicación correctamente, se ha portado todo el código a la librería flask-sqlalchemy trabajada en clase. Esto nos permite crear los test unitarios correctamente, sin tener que trabajar con requests. 

Por otro lado, en la aplicación tenían un único dockerfile para todos los microservicios y se montaba como volumen correspondiente en el compose. Para el desarrollo de los tests se ha creado un [dockerfile parametrizado](../flask_app/Dockerfile.prod) que realiza el COPY para garantizar un **build inmutable en producción**. Se ha dividido el compose en dos. Uno para test y otro para producción. El de producción solo se utiliza en AWS. Se diferencia en dos cosas: Cada uno utiliza una base de datos diferente en cada servicio y en el de test se montan los srcs como volumen para agilizar el testeo en local.

### **Test realizados**

Los test unitarios realizados **cubren 5 servicios**: auth, delivery, machine, payment, order. Dentro de cada servicio se encuentra una carpeta test con los fixtures y los tests.


Se han realizado los siguientes test:

**auth**:

- test_view_clients: Probar la creación de clientes.
- test_check_empty: Probar el inicio de la bd.


**delivery**:

- test_get_deliveries: Probar que  se devuelven deliveries.

**machine**:

- test_view_status: Probar que se devuelve el estado de la máquina.

**payment**:

- test_perfrom_deposit: Probar que un usuario puede añadir dinero.
- test_get_payment: Probar que el usuario puede ver su dinero correctamente.


**order**:

- test_get_orders: Probar que se obtienen todas las orders.
- test_get_order: Probar a obtener un order específico (**Creado para el  TDD de la nueva funcionalidad**) 


Quería destacar ciertas características de los tests:

Con el siguiente fixture en cada test se recrea la base de datos, aislando cada test.

![](img/fixture.png)

En el los test de auth se usa el siguiente fixture para crear un usuario cuando se necesita.

![](img/add_userfix.png)


Con este fixture generemos una aplicación en segundo plano que nos da un cliente de test para probar las rutas de la api REST.

![](img/app_fix.png)



### **Mocking**

Para la realización del test test_get_order se ha realizado el mock de la creación de un order mediante saga y la autenticación del jwt. Ver [aquí](../flask_app/order/test/test_order.py)

Mockeamos la lógica interna de la app para que entienda que el jwt que le pasamos es válido y para que cree una order como accepted en vez de llamar a la saga.



###  **Coverage**

Se ha obtenido un coverage final de alrededor del 55% en los test unitarios. Se lanzan los test en cada contenedor de cada servicio. Por tanto, para computar este valor tenemos que obtener los valores totales de cada output y hacer la media. Para esto se ha realizado un script de bash que se puede encontrar [aquí](../ci_coverage_check.sh).

La lógica de este script consiste en exportar la ejecución de los docker execs de coverage a un [fichero](../coverage.txt) y mediante sed y expresiones regulares obtener los valores y computar la media. Además este script printea la frase:


    echo "The total coverage is $AVG_COV"

Esto permite al pipeline de gitlab encontrar el valor en el output mediante expresiones regulares para poder mostrar el valor medio en un badge.

![](img/cov.png)

Este valor también se podría utilizar para condicionar que se pasen los test en base al coverage. Valdría con devolver un exit code fallido en este script si no se cumple la condición. 


## Smoke Test Suite

Se han creado 3 tests que interactúan con varios servicios para probar la integración entre ellos. Los servicios que interactúan son auth, order, payment, delivery y machine.

- **test_create_order**: Probar que la saga crea un order correctamente cuando se cumplen todos los requistos.(jwt válido, el usuario tiene dinero...)

- **test_create_order_fails**: Probar que si el jwt es incorrecto no se crea un order.

- **test_cancel_order**: Probar que si se crea un order es posible cancelarlo usando la saga correspondiente.

Estos test estan disponibles en la carpeta `flask_app/STS`

El funcionamiento es el siguiente.

- Se levantan los contenedores de la aplicación.
- Se levanta un contenedor simple que contiene y lanza la STS (Dockerfile creado [aquí](../flask_app/STS/Dockerfile))
- Este contenedor ejecuta los test con pytest que lanzan request contra el api-gateway de la aplicación (haproxy).
- En base a las responses de los requests y a GETs a la app realizamos la comprobaciones necesarias.



## Funcionalidad nueva usando TDD 

Como ya he mencionado la funcionalidad nueva es obtener un order especifico. Esta funcionalidad ya estaba, por tanto, la he  eliminado en [este commit](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/-/commit/524c367a3ef6a2195f96513267577e910256c263)

Para marcar lo que significa el TDD, he forzado que los pasos que se siguen en TDD en commits especificos. 

- Primero se crea el test y vemos en el pipeline que no pasa porque no esta implementado o faltan cosas.

- Se arreglan esos fallos y la nueva funcionalidad ha sido implmentada y testeada.

![](img/commits.png)

Estos son los commits: [Primero el test sin implementar funcionalidad](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/-/commit/a77db4a7bb27ef2697e41495dbd4b8ce4ca6b51c)
 y luego [Implementar la funcionalidad que pasa el test](https://gitlab.com/ivan.valdesi/ivan.valdesi-idc-p1/-/commit/57814bcdbc774d2edc2d9497d84e3b9ed5e4c54b) 

## Pipeline

El pipeliene de CI esta formado por 3 stages que se han ido desarrollando y aumentando poco a poco. Pipeline [aquí](../.gitlab-ci.yml)

### **Build**

En esta fase se crean las imagenes de nuestra aplciación tageadas como TEST y se pushean al registry de gitlab para usarlas en el test. También se crea la imagen del contenedor que realiza el test de integración.

### **Test**

- Se hace el pull de las imagenes del test anterior.
- Se levantan los microservicios que usan la bd de test. 
- Se realiza un **análisis estatico del codigo con black** en cada imagen. 
- Se calcula el coverage como hemos mencionado anteriormente.
- Se ejecuta el STS test.


### **Delivery**

Este stage solo se lanza en el merge a master. Como los tests se han pasado correctamante en este punto, se tagean como LATEST las imagenes y se suben para el despliegue.

## Deploy 

El deploy ha sido manual. Se ha modificado [el script de cloudformation](../deploy/cloud_formation.yml) del pbl para que solo cree un vpc una red pública y aprovisione un bastion en una EC2. Sobre esa ec2 se ha descargado el repo (solo para coger el script de deploy) y se ha hecho un docker login a registry.gitlab dentro del contenedor para no dejar la pass en ningun lado. Por último se ha ejecutado [este script](../deploy/deploy.sh).

Establece los tags de la imágenes de gitlab y hace el pull y el up app para poner todo en marcha. 


![](img/stack.png)
![](img/consul.png)